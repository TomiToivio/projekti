# This defines the base image used for the build.
FROM node:16-alpine

#RUN apk add --update nodejs npm

# This defines the working directory
WORKDIR /usr/app

# This copies package.json and package-lock.json files to the image
# and installs the dependencies defined in those files
COPY ./package*.json ./
RUN npm install

# This copies the src folder to the image
COPY ./app ./app

# This opens the port 3000 for external use
EXPOSE 3000
EXPOSE 5173

# This starts the app inside the container
CMD ["npm", "start"]