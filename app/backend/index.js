import express from "express";
const app = express();
import url from "url";
import path from "path";
import cors from "cors";
const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url));
const distDirectory = path.resolve(currentDirectory, "../frontend/vite-project/dist/");
console.log("Starting");
//const VERSION = "1.0.0.";
import versionData from './package.json' assert { type: "json" };
import { request } from "http";
console.log(versionData.version);
const version = versionData.version;
let todos = [{
    "id": 1,
    "item": "Placeholder",
    "status": false,
}];
app.use(cors());
app.use(express.json({extended: true, limit: '1mb'}))
app.get("/version", function (req, res) {
        const versionResponse = {
            "version": version
        };
    res.json(versionResponse);
});
app.get("/todos", function (req, res) {
    res.json(todos);
});
app.post("/todos/new", function (req, res) {
    todos.push({
        "id": req.body.id,
        "status": req.body.status,
        "item": req.body.item,
    });
    console.log(todos);
    res.json(todos);
});
app.delete("/todos/:id", function (req, res) {
    console.log(req.params.id);
    todos = todos.filter(todo => { 
        console.log(typeof todo.id);
        console.log(todo.id);
        console.log(typeof req.params.id);
        console.log(req.params.id);
        return Number(todo.id) !== Number(req.params.id); 
    });
    console.log(todos);
    res.json(todos);
});
app.use("/", express.static(distDirectory));
app.get("*", (req,res) => {
    res.sendFile("index.html", { root: distDirectory });
});
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader("Access-Control-Allow-Methods","GET,POST,PUT,PATCH,DELETE");
    res.setHeader("Access-Control-Allow-Methods","Content-Type","Authorization");
    next(); 
});
app.listen(3000);
console.log("I am listening on 3000");