import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './test/setup.js',
  },
plugins: [react()],
  server: {
    proxy: {
        "/version": "http://localhost:3000",
        "/todos": "http://localhost:3000",
    },
  },
})
