import { useState, useEffect } from 'react'
import { BiTrash } from 'react-icons/bi'
import './App.css'

let uniqueId = 100

const App = () => {
    const [list, setList] = useState([])
    const [input, setInput] = useState('')

    useEffect(() => { 
      fetchTodos();
    }, []);

    async function fetchTodos() {
      fetch("http://localhost:3000/todos/").then(response => response.json()).then(json => {
          console.log(json);
          setList(json);
      });
    }

    async function postTodo(todo) {
      fetch("http://localhost:3000/todos/new", { method: 'POST', body: JSON.stringify(todo), headers: { 'Content-Type': 'application/json' }}).then(response => response.json()).then(json => {
        console.log(json);
        setList(json);
      });
    }

    async function deleteTodo(id) {
      console.log(id);
      fetch("http://localhost:3000/todos/" + id, { method: 'DELETE', body: JSON.stringify({"id": id}), headers: { 'Content-Type': 'application/json' }}).then(response => response.json()).then(json => {
        console.log(json);
        setList(json);
      });
    }
 
    const addItem = () => {
        if (!input) return null
        let newTodo = { id: uniqueId++, status: false, item: input };
        postTodo(newTodo);
        //console.log(newList);
        //setList(newList);
        /* setList(list.concat({ id: uniqueId++, status: false, item: input })) */
        setInput('')
    }

    const toggle = (id) => () => {
        setList(list.map(item => item.id === id ? { ...item, isDone: !item.isDone } : item))
    }

    const remove = (id) => () => {
        deleteTodo(id);
        //setList(newList);
        //setList(list.filter(item => item.id !== id))
    }

    return <div className='App'>
        <input role="input" id="todoInput" value={input} onChange={(event) => setInput(event.target.value)} />
        <button role="button" id="todoButton" onClick={addItem}>Add</button>
        {list.map(({ id, isDone, item }) => {
            return <div key={id} className='item'>
                <span role="listitem" className={isDone ? 'done' : ''}
                    onClick={toggle(id)}>
                    {item}
                </span>
                <BiTrash className='icon' onClick={remove(id)} />
            </div>
        })}

    </div>
}

export default App
